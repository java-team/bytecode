package test;

public class Runner {
    public static void main(String[] args) throws Exception {
        if(args.length != 1) {
	  throw new Exception("Use: test.Runner runnableClass");
	}
	Class cl = Class.forName(args[0]);
	Runnable r = (Runnable) cl.newInstance();
	r.run();
    }
}
